from django.contrib import admin
from django.urls import path
from django.http import HttpResponse
from django.shortcuts import render

app_name = 'termo'

def view(request):
    return render(request, 'index.html')

urlpatterns = [
    path('', view ),
]
