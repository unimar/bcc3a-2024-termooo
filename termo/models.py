from django.db import models
from django.conf import settings
from datetime import timedelta

# Create your models here.


class Word(models.Model):
    content = models.CharField(
        max_length=5,
    )


class Pool(models.Model):

    datetime = models.DateTimeField(

    )

    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
        null=True,
    )

    word = models.ForeignKey(
        Word,
        on_delete=models.CASCADE,
    )


def get_word_of_day(date):
    try:
        return Pool.objects.filter(
            datetime__lte=date + timedelta(days=1),
        ).get()
    except:
        word, _ = Word.objects.get_or_create(content='teste')
        return Pool.objects.create(
            word=word,
            datetime=date,
        )
