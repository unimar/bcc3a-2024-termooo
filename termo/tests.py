import unittest
from django.utils import timezone
from django.test import TestCase
from termo.models import get_word_of_day
from datetime import timedelta

# Create your tests here.


class WordTest(TestCase):

    def test_get_word_of_day_is_not_none(self):
        now = timezone.now()
        a = get_word_of_day(now)
        self.assertIsNotNone(a)

    def test_is_the_same_word(self):
        now = timezone.now()
        a = get_word_of_day(now)
        b = get_word_of_day(now)
        self.assertEqual(a, b)

    @unittest.skip('test')
    def test_diff_word_diff_days(self):
        now = timezone.now()
        a = get_word_of_day(now)
        b = get_word_of_day(now + timedelta(days=2))
        self.assertNotEqual(a, b)
